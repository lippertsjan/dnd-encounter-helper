case class MonsterGroup(val count: Int,
                  val monster: Monster)
object MonsterGroup {
  def apply(t: Seq[String]):MonsterGroup = 
    MonsterGroup(t(0).toInt, Monster("NN", t(1).toInt))
}