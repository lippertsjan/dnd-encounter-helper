case class CliArgs(val playerCount: Int,
                   val playerLevel: Int,
                   val encounter: Seq[MonsterGroup])

object CliArgs {
  def apply(args: Array[String]): CliArgs = {
    val encounter = 
      args.toSeq
        .drop(2)
        .sliding(2,2)
        .map(t => MonsterGroup(t))
        .toSeq

    CliArgs(args(0).toInt,
            args(1).toInt,
            encounter)
  }
}