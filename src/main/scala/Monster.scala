
case class Monster(val name: String,
                   val xp: Int
)

/*
case class Monster(val name: String,
                   val size: Size,
                   val armorClass: Int,
                   val hitPoints: Dices: Int,
                   val speed: Int,
                   val burrowSpeed: Int = 0,
                   val climbSpeed: Int = 0,
                   val flySpeed: Int = 0,
                   val swimSpeed: Int = 0,
                   val str: Int,
                   val dex: Int,
                   val con: Int,
                   val int: Int,
                   val wis: Int,
                   val cha: Int,
                   val challenge: Int,
                   val xp: Int
)*/