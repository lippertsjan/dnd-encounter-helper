object Size extends Enumeration  
{
  type Size = Value

  val tiny = Value(4)
  val small = Value(6)
  val medium = Value(8)
  val large = Value(10)
  val huge = Value(12)
  val gargantuan = Value(20)
}
