case class XpThreshold(val easy: Int,
                       val medium: Int,
                       val hard: Int,
                       val deadly: Int){
    def *(i: Int): XpThreshold 
      = XpThreshold(i * easy,
                    i * medium,
                    i * hard,
                    i * deadly)
}
object XpThreshold {
  implicit class MultiplicationExtensionInt(val i: Int) {
    def *(x: XpThreshold): XpThreshold 
      = XpThreshold(i * x.easy,
                    i * x.medium,
                    i * x.hard,
                    i * x.deadly)

  }
}