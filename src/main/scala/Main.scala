object Main extends App {
  def determineDifficulty(pxt: XpThreshold,
                          adaptedXp: Double) {
    println(partyXpThreshold)
    println(adaptedXp)

    val easy = partyXpThreshold.easy
    val medium = partyXpThreshold.medium
    val hard = partyXpThreshold.hard

    if(adaptedXp <= pxt.easy){
      print(s"Easy: $adaptedXp")
    } else if(adaptedXp <= pxt.medium){
      print(s"Medium: $adaptedXp")
    } else if(adaptedXp <= pxt.hard){
      print(s"Hard: $adaptedXp")
    } else if(adaptedXp <= pxt.deadly){
      print(s"Deadly: $adaptedXp")
    } else {
      print(s"Over Deadly: $adaptedXp") 
    }
    println(s" $pxt")
  }
  def determineXpPerPlayer(encounterXp: Double, 
                           playerCount: Int) {
    val xpPerPlayer = (encounterXp/playerCount).toInt;
    println(s"$xpPerPlayer XP per player.")
  }

  val cargs = CliArgs(args)

  val xpThresholdsByCharacterLevel = Map(
    1 -> XpThreshold(  25,   50,   75,   100),
    2 -> XpThreshold(  50,  100,  150,   200)
  )
  val encounterMultipliers: Map[Int, Double] = Map(
     0 -> 0,
     1 -> 1,
     2 -> 1.5,
     3 -> 2,
     4 -> 2,
     5 -> 2,
     6 -> 2,
     7 -> 2.5,
     8 -> 2.5,
     9 -> 2.5,
    10 -> 2.5,
    11 -> 3,
    12 -> 3,
    13 -> 3,
    14 -> 3
  )

  val playerCount = cargs.playerCount
  val playerLevel = cargs.playerLevel

  val singlePlayerXpThreshold = xpThresholdsByCharacterLevel(playerLevel)

  val partyXpThreshold = playerCount*singlePlayerXpThreshold 

  val encounterXp: Int = cargs.encounter.map(mg => mg.count * mg.monster.xp).sum
  val encounterSize = cargs.encounter.map(mg => mg.count).sum
  val encounterMultiplier: Double = encounterMultipliers.getOrElse(encounterSize, 4)
  val adaptedXp = encounterXp * encounterMultiplier;

  determineDifficulty(partyXpThreshold, adaptedXp)
  determineXpPerPlayer(encounterXp, playerCount)
}